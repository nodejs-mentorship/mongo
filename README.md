# Mongo

## Task

Create your own express web server and
use express Router as middleware to route CRUD request for eg. Student entity
(done in Workshop 10). Implement routes logic to work with MongoDB, Mongoose
and MongoClient.
